# Messenger Server
Клиент для сервера: https://gitlab.com/aleshkashell/messenger-client.git

## Содержание

- [Запуск](#Запуск)
- [Урок 1](#Урок-1)

## Запуск
```bash
git clone https://gitlab.com/aleshkashell/messenger-server.git && cd messenger-server
pip install -r requirements.txt
python server.py
```

## Урок 1

### Что сделано:

- Исправлена ошибка обработки сообщений от клиента
(если сообщения отправлялись с небольшим инетрвалом, то они сливались в одно на сервере)
- Добавлено README