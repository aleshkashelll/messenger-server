from cx_Freeze import setup, Executable

executables = [Executable('server.py')]

build_exe_options = {"packages": ["sqlalchemy"]}

setup(name='MessageServer',
      version='0.0.1',
      description='GeekBrains message server!',
      options={
          "build_exe": build_exe_options
        },
      executables=executables)
