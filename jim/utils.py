import json
import binascii
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

from .config import PACKAGE_LENGTH, ENCODING


def decrypt_message(data, key):
    if key:
        session_key = key['session_key']
        iv = key['iv']
        decipher = AES.new(session_key, AES.MODE_CFB, iv=iv)
        return decipher.decrypt(data)
    return data


def process_message(bdata):
    try:
        text = bdata.decode(ENCODING)
        if text == "":
            return None
        else:
            json_data = json.loads(text)
    except json.JSONDecodeError:
        return {"error_data": bdata}
    except UnicodeDecodeError:
        return {"error_data": bdata}
    if isinstance(json_data, dict):
        return json_data
    else:
        return None


def get_message(client, key=None, queue=None):
    """
    Получение сообщений из сокета.

    :param client: сокет
    :param key: AES ключ, если шифрование установлено
    :param queue: Очередь сообщений
    :return: None
    """
    data = client.recv(PACKAGE_LENGTH)
    data = binascii.unhexlify(data)
    if data == b'':
        return None
    if isinstance(data, bytes):
        bmessage = decrypt_message(data=data, key=key)
        if queue:
            for i in bmessage.split(b'\n'):
                message = process_message(i)
                if message:
                    queue.put({"message": message, "client": client})
        else:
            return process_message(bmessage)
    else:
        return None


def send_message(sock, data, key=None):
    """
    Отправка сообщений в сокет

    :param sock: сокет
    :param data: данные
    :param key: AES ключ, если шифрование установлено
    :return: None
    """
    message = json.dumps(data)
    enc_message = f'{message}\n'.encode(ENCODING)
    if key:
        session_key = key['session_key']
        iv = key['iv']
        cipher = AES.new(session_key, AES.MODE_CFB, iv=iv)
        enc_message = cipher.encrypt(enc_message)
    enc_message = binascii.hexlify(enc_message)
    sock.send(enc_message)


if __name__ == '__main__':
    data = b'Some simple text'
    session_key = get_random_bytes(32)
    cipher = AES.new(session_key, AES.MODE_CBC)
    iv = cipher.iv
    ciphertext = cipher.encrypt(pad(data, AES.block_size))
    session_key = binascii.hexlify(session_key).decode('ascii')
    iv = binascii.hexlify(cipher.iv).decode('ascii')
    key = {"session_key": session_key, "iv": iv}
    print(f'Key: {key}')
    print(key['session_key'].encode('ascii'))
    print(f'Data: {data}')
    print(f'Session key: {session_key}')
    print(f'Cipher: {cipher}')
    print(f'IV: {iv}')
    print(f'Cipher text: {ciphertext}')
    #
    decipher = AES.new(binascii.unhexlify(session_key), AES.MODE_CBC,
                       iv=binascii.unhexlify(iv))
    decrypt_data = unpad(decipher.decrypt(ciphertext), AES.block_size)
