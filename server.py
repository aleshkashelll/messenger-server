# -*- coding: utf-8 -*-
import argparse
import sys
import os
import logging
import threading
import select
import time
import binascii
import json
import tempfile
from datetime import datetime
from queue import Queue
from socket import socket, AF_INET, SOCK_STREAM

from PyQt5.QtWidgets import QHeaderView
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP

from jim.config import PROTO, JIM, USER_STATUS, MAX_CONNECTIONS, \
    LOGGING_LEVEL, DEF_LISTEN_ADDR, DEF_LISTEN_PORT
from jim.utils import send_message, get_message
from loggers import init_logging
from metaclasses.meta import ServerVerifier
from descriptors.descriptors import ServerPort
from storage.server_db import Storage
from files.server_main import Ui_MainWindow


init_logging(tempfile.gettempdir())

DEBUG = False
logger = logging.getLogger('server')
logger.setLevel(logging.DEBUG)
if DEBUG:
    formatter = logging.Formatter("%(asctime)-25s %(filename)-30s "
                                  "%(levelname)-10s %(message)s")
    console = logging.StreamHandler()
    console.setLevel(LOGGING_LEVEL)
    console.setFormatter(formatter)
    logger.addHandler(console)


class MessageServer(metaclass=ServerVerifier):
    port = ServerPort()
    """Проверка порта на валидность"""
    answer_queue = Queue()
    """Очередь обработки ответов клиентов"""
    input_messages = Queue()
    """Очередь входящих сообщений"""
    authorized = list()
    """Авторизованные клиенты (username)"""
    names = dict()
    """Все пользователи (username)"""
    clients = list()
    """Все подключенные сокеты"""

    def __init__(self, address, port):
        self.port = port
        self.address = address
        self._init_net()
        self.initial_storage = Storage()
        self._init_encryption()
        users = self.initial_storage.get_all_clients()
        for user in users:
            user.status = USER_STATUS.OFFLINE
        self.initial_storage.session.commit()

        self.thread_input_messages = threading.Thread(
            target=self.thread_proc_message)
        self.thread_input_messages.daemon = True
        self.thread_input_messages.start()

        self.thread_answer_queue = threading.Thread(
            target=self.process_answer_queue)
        self.thread_answer_queue.daemon = True
        self.thread_answer_queue.start()

    def _init_net(self, timeout=0.5):
        """
        Инициализация сети

        :param timeout: Таймаут подключени
        :return: None
        """
        self._socket = socket(AF_INET, SOCK_STREAM)
        try:
            self._socket.bind((self.address, self.port))
        except OSError:
            print("Address already in use")
            logger.error(f'Address already in use')
            exit(1)
        self._socket.listen(MAX_CONNECTIONS)
        self._socket.settimeout(timeout)

    def _init_encryption(self):
        """
        Установка RSA ключей для изначального обмена симметричными ключами

        :return: None
        """
        key = RSA.generate(2048)
        self.private_key = key.export_key()
        self.public_key = key.publickey().export_key()

    def disconnect_client(self, client):
        """
        Отключить клиента

        :param client: Сокет
        :return: None
        """
        username = self.get_username_by_socket(client)
        if username:
            user = self.storage.get_client_by_name(username)
            if user:
                user.status = USER_STATUS.OFFLINE
                try:
                    self.storage.register_action(user, client.getpeername()[0],
                                                 PROTO.EXIT)
                except OSError:
                    logger.debug(f'Client {username} disconected')
        self.clients.remove(client)
        logger.debug(f'Client {client} disconected')

    def get_key_by_socket(self, client):
        """
        Ключ шифрования клиента по сокету

        :param client: Сокет
        :return: bytes: Ключ шифрования
        """
        for key, value in self.names.items():
            if client == value['client']:
                return value['key']

    def get_username_by_socket(self, client):
        """
        Получить логин пользователя по сокету

        :param client: Сокет
        :return: str: Имя пользователя
        """
        for key, value in self.names.items():
            if client == value['client']:
                return key

    def process_answer_queue(self):
        """Обработка и отправка ответов пользователям"""
        while True:
            if not self.answer_queue.empty():
                item = self.answer_queue.get()
                if item is None:
                    break
                send_message(sock=item['client'], data=item['message'],
                             key=item.get('key'))
                self.answer_queue.task_done()
            else:
                time.sleep(0.1)
        self.answer_queue.task_done()

    def proc_message(self, message, client):
        """Обоработка входящих сообщений."""
        # logger.debug(f'Client message: {message}')
        if not message:
            self.disconnect_client(client)
        else:
            try:
                if JIM.TIME not in message or JIM.SENDER not in message:
                    raise KeyError
                if message[JIM.ACTION] == PROTO.PRESENCE:
                    logger.debug(f'Presence message OK')
                    # Пробуем авторизовать клиента

                    # Отправляем сообщение, что все ок
                    self.answer_queue.put({"client": client,
                                           "message": {PROTO.RESPONSE: 200},
                                           "key": self.names[
                                               message[JIM.SENDER]]['key']})

                    # Переименовываем клиента у себя, регистрируем его действия
                    username = message[JIM.AUTH_DATA][PROTO.USERNAME]
                    self.names[username] = {"client": client,
                                            'key': self.names[
                                                message[JIM.SENDER]]['key']}
                    # Удаляем временного пользователя
                    self.names.pop(message[JIM.SENDER])
                    user = self.storage.register_client(username)
                    self.storage.register_action(user, client.getpeername()[0],
                                                 message[JIM.ACTION])
                    user.last_ip_address = client.getpeername()[0]
                    user.status = USER_STATUS.ONLINE
                    user.last_presence_time = datetime.now()
                    self.storage.session.commit()
                elif message[JIM.ACTION] == PROTO.MESSAGE:
                    user = self.storage.get_client_by_name(message[JIM.SENDER])
                    if user:
                        self.storage.register_action(
                            user, client.getpeername()[0],
                            message[JIM.ACTION],
                            message=message[PROTO.MESSAGE_TEXT],
                            recepients=message[JIM.RECEIVER])
                        try:
                            sock = self.names[message[JIM.RECEIVER]]['client']
                        except KeyError:
                            logger.debug(f'User {message[JIM.RECEIVER]}'
                                         f' not online')
                            return
                        try:
                            self.answer_queue.put({"client": sock,
                                                   "message": message})
                        except OSError:
                            self.names.pop(message[JIM.RECEIVER], None)
                elif message[JIM.ACTION] == PROTO.ADD_CONTACT:
                    user = self.storage.get_client_by_name(message[JIM.SENDER])
                    friend = self.storage.get_client_by_name(
                        message[JIM.FRIEND])
                    if user and friend:
                        self.storage.add_friend(client=user, friend=friend)
                elif message[JIM.ACTION] == PROTO.DEL_CONTACT:
                    user = self.storage.get_client_by_name(message[JIM.SENDER])
                    friend = self.storage.get_client_by_name(
                        message[JIM.FRIEND])
                    if user and friend:
                        self.storage.del_friend(client=user, friend=friend)
                elif message[JIM.ACTION] == PROTO.GET_CONTACTS:
                    user = self.storage.get_client_by_name(message[JIM.SENDER])
                    if user:
                        all_clients = self.storage.get_all_clients()
                        data = dict()
                        friends = self.storage.get_friends(user)
                        for i in all_clients:
                            if i in friends:
                                is_friend = True
                            else:
                                is_friend = False
                            data[i.login] = {"status": i.status,
                                             "is_friend": is_friend}
                        logger.debug(f'Friends for {user.login}: {data}')
                        answer = {PROTO.RESPONSE: 202,
                                  JIM.ACTION: PROTO.UPDATE_CONTACTS,
                                  JIM.TIME: time.time(),
                                  JIM.CONTACTS: data}
                        self.answer_queue.put({"client": client,
                                               "message": answer})
                elif message[JIM.ACTION] == PROTO.KEY_EXCHANGE:
                    # Верменное имя, пока пользователь не залогинится
                    random_bytes = binascii.hexlify(os.urandom(32))
                    random_name = f"temp_{random_bytes.decode('ascii')}"
                    # Создание симметричного ключа для сессии с этим клиентом
                    session_key = get_random_bytes(32)
                    cipher = AES.new(session_key, AES.MODE_CBC)
                    # В JSON нельзя байты
                    key = {"session_key": binascii.hexlify(
                        session_key).decode('ascii'),
                           "iv": binascii.hexlify(cipher.iv).decode('ascii')}
                    # logger.debug(f'Hexlify key: {key}')
                    byte_key = json.dumps(key).encode('ascii')
                    # logger.debug(f'Byte key: {byte_key}')
                    # Открытый RSA ключ клиента
                    recipient_key = binascii.unhexlify(message[JIM.KEY])
                    # Шифруем aes ключ и iv
                    # logger.debug(f'Recipient key: {recipient_key}')
                    rsa_recipient_key = RSA.import_key(recipient_key)
                    cipher_rsa = PKCS1_OAEP.new(rsa_recipient_key)
                    # logger.debug(f'Client key imported')
                    enc_session_key = cipher_rsa.encrypt(byte_key)
                    # logger.debug(f'Encrypted key: {enc_session_key}')
                    # Переводим все это дело в строку,
                    # чтобы можно было конвертировать JSON
                    hex_key = binascii.hexlify(enc_session_key).decode('ascii')
                    # logger.debug(f'Hex key: {hex_key}')
                    answer = {PROTO.RESPONSE: 200,
                              JIM.ACTION: PROTO.KEY_EXCHANGE,
                              JIM.TIME: time.time(),
                              JIM.RECEIVER: random_name, JIM.KEY: hex_key}
                    self.names[random_name] = {'client': client, "key": {
                        "session_key": session_key, "iv": cipher.iv
                    }}
                    self.answer_queue.put({"client": client,
                                           "message": answer})

                elif message[JIM.ACTION] == PROTO.EXIT:
                    user = self.storage.get_client_by_name(message[JIM.SENDER])
                    if user:
                        self.storage.register_action(user,
                                                     client.getpeername()[0],
                                                     message[JIM.ACTION])
                        user.status = USER_STATUS.OFFLINE
                        self.storage.session.commit()
                    logger.info(f'Client "{client}" disconnected')
                    self.names.pop(message[JIM.SENDER], None)
                    self.clients.remove(client)
                else:
                    logger.warning(f'Incorrect message: {message}')
                    self.answer_queue.put({"client": client, "message": {
                        PROTO.RESPONSE: 400,
                        PROTO.ERROR: 'Bad request',
                        PROTO.DESCRIPTION: f'Unknown action '
                                           f'{message[JIM.ACTION]}'
                    }})
            except KeyError:
                logger.debug(f'Incorrect message: {message}')
                self.answer_queue.put(
                    {"client": client,
                     "message": {PROTO.RESPONSE: 400,
                                 PROTO.ERROR: 'Bad request',
                                 PROTO.DESCRIPTION: 'Incorrect message'}})

    def run(self):
        """Прием новых соединений"""
        self.storage = Storage()
        logger.info('Server running')
        while True:
            # Проверка вспомогательных потоков на существование
            if not self.thread_input_messages.is_alive():
                logger.error(f"Input message handler is dead.")
                break
            if not self.thread_answer_queue.is_alive():
                logger.error(f'Answer handler is dead.')
                break
            try:
                client, address = self._socket.accept()
                presence = get_message(client)
                self.input_messages.put({"message": presence,
                                         "client": client})
            except OSError:
                pass
            else:
                logger.info(f'Client connected: {address}')
                self.clients.append(client)
            in_clients = list()
            out_clients = list()
            err_message = list()
            try:
                if self.clients:
                    in_clients, out_clients, err_message = select.select(
                        self.clients, self.clients, [], 0)
            except OSError:
                pass
            if in_clients:
                for in_client in in_clients:
                    try:
                        # logger.debug(f'Trying get message from client.
                        # Key: {self.get_key_by_socket(in_client)}')
                        get_message(client=in_client,
                                    key=self.get_key_by_socket(in_client),
                                    queue=self.input_messages)
                        # self.input_messages.put({"message": get_message(
                        #     in_client, key=self.get_key_by_socket(in_client)),
                        #                          "client": in_client})
                    except OSError:
                        self.disconnect_client(in_client)

    def thread_proc_message(self):
        """Запуск потока обработки сообщений"""
        while True:
            if not self.input_messages.empty():
                item = self.input_messages.get()
                if item is None:
                    break
                message = item['message']
                client = item['client']
                self.proc_message(message=message, client=client)
                self.input_messages.task_done()
            else:
                time.sleep(0.1)
        self.input_messages.task_done()


class QPlainTextEditLogger(logging.StreamHandler):
    """Класс для отображения логов в интерфейсе"""
    def __init__(self, parent):
        super().__init__()
        self.widget = parent.logView
        self.widget.setReadOnly(True)
        self.widget.setText("Hello")

    def emit(self, record):
        msg = self.format(record)
        print(msg)
        self.widget.append(msg)


class Interface(QtWidgets.QMainWindow, Ui_MainWindow):
    """Графический интерфейс"""
    def __init__(self, server, parent=None):
        self.server = server
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        # uic.loadUi('files/server_main.ui', self)
        self.prepare()

    def prepare(self):
        """Подготовка таймеров"""
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_contacts)
        self.timer.start(1000)

    def run(self):
        """Запуск серверной логики"""
        logger.info('Server started')
        self.server.run()

    def update_contacts(self):
        """Обновление контактов в интерфейсе"""
        contacts = self.server.initial_storage.get_all_clients()
        headers = ['Login', 'IP address', 'Status', 'Presence time',
                   'Register time']
        model = QStandardItemModel()
        model.setHorizontalHeaderLabels(headers)
        for user in contacts:
            login = user.login
            reg_time = user.register_date
            status = user.status
            ip = user.last_ip_address
            presence = user.last_presence_time

            login = QStandardItem(login)
            login.setEditable(False)
            reg_time = QStandardItem(reg_time.strftime("%d/%m/%Y, %H:%M:%S"))
            reg_time.setEditable(False)
            status = QStandardItem(status)
            status.setEditable(False)
            ip = QStandardItem(ip)
            ip.setEditable(False)
            presence = QStandardItem(presence.strftime("%d/%m/%Y, %H:%M:%S"))
            presence.setEditable(False)
            model.appendRow([login, ip, status, presence, reg_time])

        self.clientList.setModel(model)
        # self.clientList.resizeColumnsToContents()
        # self.clientList.resizeRowsToContents()
        # self.clientList.horizontalHeader().setStretchLastSection(True)
        self.clientList.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch)
        # self.clientList.


def get_values():
    """Парсер командной строки"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--address',
                        help='Адрес, на котором слушает сервер',
                        default=DEF_LISTEN_ADDR)
    parser.add_argument('-p', '--port', type=int,
                        help='Порт, на котором слушает сервер',
                        default=DEF_LISTEN_PORT)
    args = parser.parse_args()
    logger.info(f'Параметры командной строки: {args}')
    if args.port < 1024 or args.port > 65535:
        logger.error(f'Некорректный порт: {args}')
        print("Неккоректный порт. Введите порт в диапзоне 1024-65535")
        exit(-1)
    return args


def main():
    """Запуск консольного сервера"""
    args = get_values()
    server = MessageServer(args.address, args.port)
    logger.info(f'Start server')
    print(f'Server started')
    server.run()


def run_ui():
    """Запуск интерфейса"""
    args = get_values()
    server = MessageServer(args.address, args.port)
    app = QtWidgets.QApplication(sys.argv)
    window = Interface(server=server)

    qt_log = QPlainTextEditLogger(window)
    formatter = logging.Formatter("%(asctime)-25s %(filename)-30s "
                                  "%(levelname)-10s %(message)s")
    qt_log.setLevel(logging.DEBUG)
    qt_log.setFormatter(formatter)
    logger.addHandler(qt_log)
    logger.info("Message")

    window.show()
    t = threading.Thread(target=window.run)
    t.daemon = True
    t.start()
    sys.exit(app.exec_())


if __name__ == '__main__':
    run_ui()
