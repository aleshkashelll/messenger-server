import unittest
import time
from jim.config import JIM, PROTO


def proc_message(data):
    pass


class TestProcMessage(unittest.TestCase):
    def test_correct(self):
        data = {JIM.ACTION: PROTO.PRESENCE, JIM.TIME: time.time(),
                JIM.USER: {PROTO.ACCOUNT_NAME: 'Guest'}}
        result = {JIM.RESPONSE: 200}
        self.assertEqual(result, proc_message(data))

    def test_wrong_action(self):
        data = {JIM.ACTION: 'prrrrrrrrrrrr', JIM.TIME: time.time(),
                JIM.USER: {PROTO.ACCOUNT_NAME: 'Guest'}}
        result = {JIM.RESPONSE: 400, PROTO.ERROR: 'Bad request'}
        self.assertEqual(result, proc_message(data))

    def test_without_action(self):
        data = {JIM.TIME: time.time(), JIM.USER: {PROTO.ACCOUNT_NAME: 'Guest'}}
        result = {JIM.RESPONSE: 400, PROTO.ERROR: 'Bad request'}
        self.assertEqual(result, proc_message(data))

    def test_without_time(self):
        data = {JIM.ACTION: PROTO.PRESENCE,
                JIM.USER: {PROTO.ACCOUNT_NAME: 'Guest'}}
        result = {JIM.RESPONSE: 400, PROTO.ERROR: 'Bad request'}
        self.assertEqual(result, proc_message(data))

    def test_without_user(self):
        data = {JIM.ACTION: PROTO.PRESENCE, JIM.TIME: time.time()}
        result = {JIM.RESPONSE: 400, PROTO.ERROR: 'Bad request'}
        self.assertEqual(result, proc_message(data))

    def test_wrong_name(self):
        data = {JIM.ACTION: PROTO.PRESENCE, JIM.TIME: time.time(),
                JIM.USER: {PROTO.ACCOUNT_NAME: 'Unknown user'}}
        result = {JIM.RESPONSE: 400, PROTO.ERROR: 'Bad request'}
        self.assertEqual(result, proc_message(data))


if __name__ == '__main__':
    unittest.main()
